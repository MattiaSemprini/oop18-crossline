package it.unibo.oop.test.game.actor.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.player.PlayerImpl;
import it.unibo.oop.crossline.game.bullet.BulletBuilder;
import it.unibo.oop.crossline.game.bullet.BulletBuilderImpl;
import it.unibo.oop.crossline.game.weapon.WeaponImpl;

/**
 * Test class for Player.
 */
public class PlayerImplTest {

    /**
     * Zero value.
     */
    private static final int ZERO = 0;
    /**
     * First player.
     */
    private PlayerImpl player1;
    /**
     * Second player.
     */
    private PlayerImpl player2;
    /**
     * Same as first player.
     */
    private PlayerImpl sameAsPlayer1;
    private WeaponImpl weapon;
    /**
     * Unimplemented player.
     */
    private static final PlayerImpl UNIMPLEMENTED_PLAYER = null;
    private static final float EARTH_GRAVITY = -9.8f;
    private static final float DIFFICULTY = 5;
    private static final float BASE_BULLET_DAMAGE = 10f;
    private static final float BASE_BULLET_SPEED = 2f;
    private static final long SHOT_DELAY = 1;

    /**
     * SetUp method for instance players.
     * 
     * @throws java.lang.Exception generic exception
     */
    @Before
    public void setUp() throws Exception {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final BulletBuilder bulletBuilder = new BulletBuilderImpl().setDamage(BASE_BULLET_DAMAGE * DIFFICULTY)
                .setSpeed(BASE_BULLET_SPEED * DIFFICULTY);
        weapon = new WeaponImpl(SHOT_DELAY, bulletBuilder);
        player1 = new PlayerImpl(world, new Vector2(1, 0));
        player2 = new PlayerImpl(world, new Vector2(0, 1));
        sameAsPlayer1 = player1;
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#hashCode()}.
     */
    @Test
    public final void testHashCode() {
        assertNotEquals(player1.hashCode(), ZERO);
        assertNotEquals(player2.hashCode(), ZERO);
        assertNotEquals(player1.hashCode(), player2.hashCode());
        assertEquals("player1 equals to sameAsPlayer1", player1.hashCode(), sameAsPlayer1.hashCode());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#getCircleRadius()}.
     */
    @Test
    public final void testGetCircleRadius() {
        assertNotNull("the circle radius of player1 is not null", PlayerImpl.getCircleRadius());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#PlayerImpl(com.badlogic.gdx.physics.box2d.World, com.badlogic.gdx.math.Vector2)}.
     */
    @Test
    public final void testPlayerImpl() {
        assertNotNull("player1 is not null", player1);
        assertNotNull("player2 is not null", player2);
        assertNotNull("sameAsPlayer1 is not null", sameAsPlayer1);
        assertNull("UNIMPLEMENTED_PLAYER is null", UNIMPLEMENTED_PLAYER);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#applyDamage(float)}.
     */
    @Test
    public final void testApplyDamage() {
        final float damage = 100;
        player1.applyDamage(damage);
        assertTrue("player1's life dropped after attack", player1.isQueuedForDestruction());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        assertNotEquals(player1, player2);
        assertEquals("player1 equals to sameAsPlayer1", player1, sameAsPlayer1);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#getBody()}.
     */
    @Test
    public final void testGetBody() {
        assertNotNull("the body of player1 is not null", player1.getBody());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#getHealth()}.
     */
    @Test
    public final void testGetHealth() {
        assertNotNull("the health of player1 is not null", player1.getHealth());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#isJumping()}.
     */
    @Test
    public final void testGetJumpState() {
        assertNotNull("the jump state of player1 is not null", player1.isJumping());
        assertFalse("player1 is not jumping", player1.isJumping());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#getTeam()}.
     */
    @Test
    public final void testGetTeam() {
        assertNotNull("the team state of player1 is not null", player1.getTeam());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#getWeapon()}.
     */
    @Test
    public final void testGetWeapon() {
        assertNull("the weapon of player1 is null", player1.getWeapon());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#isQueuedForDestruction()}.
     */
    @Test
    public final void testIsQueuedForDestruction() {
        assertFalse("player1 is not queued for desctruction", player1.isQueuedForDestruction());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#isShouldShoot()}.
     */
    @Test
    public final void testIsShouldShoot() {
        assertFalse("player1 shouldn't shoot", player1.isShouldShoot());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#jump()}.
     */
    @Test
    public final void testJump() {
        final String before = player1.toString();
        player1.jump();
        final String after = player1.toString();
        assertNotEquals("player1 has jumped well", before, after);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#queueForDestruction()}.
     */
    @Test
    public final void testQueueForDestruction() {
        assertFalse("player1 is not queued for desctruction", player1.isQueuedForDestruction());
        player1.queueForDestruction();
        assertTrue("player1 is queued for desctruction", player1.isQueuedForDestruction());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#setJumpState(boolean)}.
     */
    @Test
    public final void testSetJumpState() {
        assertFalse("player1 is not jumping", player1.isJumping());
        player1.setJumpState(true);
        assertTrue("player1 is jumping", player1.isJumping());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#setShouldShoot(boolean)}.
     */
    @Test
    public final void testSetShouldShoot() {
        assertFalse("player1 shouldn't shoot", player1.isShouldShoot());
        player1.setShouldShoot(true);
        assertTrue("player1 should shoot", player1.isShouldShoot());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#setWeapon(it.unibo.oop.crossline.game.weapon.Weapon)}.
     */
    @Test
    public final void testSetWeapon() {
        assertNull("the weapon of player1 is null", player1.getWeapon());
        player1.setWeapon(weapon);
        assertNotNull("the weapon of player1 is not null", player1.getWeapon());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#shoot()}.
     * @throws InterruptedException for threading sleep
     */
    @Test
    public final void testShoot() throws InterruptedException {
        weapon.setOwner(player1);
        weapon.shoot();
        // without wait, maybe the time between one shoot and other could be zero,
        // making fail the test
        Thread.sleep(1);
        player1.setWeapon(weapon);
        player1.setShouldShoot(true);
        final String before = player1.toString();
        player1.shoot();
        final String after = player1.toString();
        assertNotEquals("player1 has shooted well", before, after);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.player.PlayerImpl#toString()}.
     */
    @Test
    public final void testToString() {
        assertNotNull("the string of player1 is not null", player1.toString());
    }

}
