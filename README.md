# Crossline

## Instructions
1. Download and install **Java SE Runtime Environment 8** from [here](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) if you don't already have it
2. Download **crossline.jar** from this repository's downloads section
3. Now you can execute **crossline.jar** by either double clicking it or running the following command: `$ java -jar crossline.jar`

## Libraries used
* [JUnit 4](https://github.com/junit-team/junit4)
* [libGDX](https://github.com/libgdx/libgdx)

## Programs used
* [LibGDX Texture Packer](https://github.com/crashinvaders/gdx-texture-packer-gui)
* [Tiled](https://www.mapeditor.org/)
* [ShoeBox](https://renderhjs.net/shoebox/)

## Credits
* [Font](https://www.dafont.com/it/quantum-4.font) by Gregory "Sesohq" Ortiz
* [Tileset](https://0x72.itch.io/16x16-industrial-tileset) by 0x72

## Authors
* [Bagli Matteo](https://bitbucket.org/MatteoZenoBagli/)
* [Casadei Mirco](https://bitbucket.org/mircocasadei3/)
* [Righetti Franco](https://bitbucket.org/FrancoRighetti/)
* [Semprini Mattia](https://bitbucket.org/MattiaSemprini/)