package it.unibo.oop.crossline.drawabale.product;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import it.unibo.oop.crossline.drawabale.AnimationState;

/**
 * Image of inanimate things.
 */
public class SkinProductInanimateImpl implements SkinProduct {

    private AnimationState state;

    @Override
    public void drawOnSpriteBatch(final SpriteBatch batch, final Vector2 pos) {


    }

    @Override
    public void dispose() {


    }
    @Override
    public final AnimationState getState() {
        return this.state;
    }

    @Override
    public final void setState(final AnimationState state) {
        this.state = state;
    }

    @Override
    public void swichFrameByTime() {

    }

}
