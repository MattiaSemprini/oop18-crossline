package it.unibo.oop.crossline.drawabale.product.attributes.idle;

import it.unibo.oop.crossline.drawabale.product.SkinProduct;

/**
 *
 * Idle State.
 *
 */
public interface Idle {

    /**
     *
     * @return SKin Product in idle.
     */
    SkinProduct idle();

}
