package it.unibo.oop.crossline.drawabale.product.attributes.jump;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

import it.unibo.oop.crossline.drawabale.AnimationState;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;
import it.unibo.oop.crossline.drawabale.product.SkinProductAnimatedImpl;

/**
*
* Jump Player.
*
*/
public class JumpPlayerImpl implements Jump {

    private final SkinProduct animation;

    /**
     *
     * create player jump.
     */
    public JumpPlayerImpl() {
        super();
        this.animation =  new SkinProductAnimatedImpl(AnimationState.getAtlas(), "jumpP",
                AnimationState.SPEED, PlayMode.LOOP);
        this.animation.setState(AnimationState.IS_JUMPING);
    }

    @Override
    public final SkinProduct jump() {
        this.animation.setState(AnimationState.IS_JUMPING);
        return animation;
    }

}
