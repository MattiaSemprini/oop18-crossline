package it.unibo.oop.crossline.drawabale.factory;

import it.unibo.oop.crossline.drawabale.AnimationState;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;

/**
 * Create skin of various type.
 * @param <X> type of skin to create.
 *
 */
public interface SkinFactory<X> {

    /**
     * @param entity type of skin to draw.
     *@return {@link it.unibo.oop.crossline.drawabale.product.SkinProduct}
     */
    SkinProduct createSkin(X entity);

    /**
     *Set the product to draw before draw.
     *@param a {@link AnimationState} the state to change.
     *@param entity {@link X} type of skin to draw.
     *@return Skin to draw.
     */
    SkinProduct createSkinWithState(X entity, AnimationState a);
}
