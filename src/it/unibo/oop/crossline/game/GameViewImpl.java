package it.unibo.oop.crossline.game;

import java.util.List;
import java.util.Optional;
import org.lwjgl.util.Dimension;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.Body;
// import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import it.unibo.oop.crossline.drawabale.AnimationState;
import it.unibo.oop.crossline.drawabale.factory.SkinFactoryEntityImpl;
import it.unibo.oop.crossline.drawabale.factory.SkinFactoryObjectImpl;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;
import it.unibo.oop.crossline.game.actor.player.Player;
import it.unibo.oop.crossline.game.actor.robot.Robot;
import it.unibo.oop.crossline.game.bullet.Bullet;
import it.unibo.oop.crossline.game.camera.Camera;
import it.unibo.oop.crossline.game.camera.CameraImpl;
import it.unibo.oop.crossline.game.wave.Wave;

/**
 * This class is the game view, which manages the rendering of the game.
 */
public class GameViewImpl extends Game implements GameView {

    private static final float BACKGROUND_RED = 0.114f;
    private static final float BACKGROUND_GREEN = 0.129f;
    private static final float BACKGROUND_BLUE = 0.176f;
    private static final Dimension VIEWPORT_SIZE = new Dimension(512, 288);
    private static final float UNITY_SCALE = 1 / 16f;



    private final Camera camera;
    // private final Box2DDebugRenderer debugRenderer;
    private Optional<OrthogonalTiledMapRenderer> mapRenderer;
    private Optional<World> world;
    private Optional<Player> player;
    private Optional<List<? extends Robot>> enemies;

    private Optional<SkinProduct> skinPlayer;
    private Optional<SkinProduct> skinEnemy;
    private Optional<SkinProduct> skinBullet;




    private final SpriteBatch spriteBatch;

    /**
     * Initialize the game view.
     */
    public GameViewImpl() {
        super();
        camera = new CameraImpl(VIEWPORT_SIZE);
        camera.setZoom(UNITY_SCALE);
        // debugRenderer = new Box2DDebugRenderer();
        mapRenderer = Optional.empty();
        spriteBatch = new SpriteBatch();
        world = Optional.empty();
        enemies = Optional.empty();
        skinPlayer = Optional.empty();
        skinBullet = Optional.empty();

    }

    @Override
    public final void dispose() {
        super.dispose();
        skinEnemy.get().dispose();
        skinPlayer.get().dispose();
        skinBullet.get().dispose();
    }

    @Override
    public final void render(final float delta) {

        final Array<Body> bodies = new Array<>(world.get().getBodyCount());
        world.get().getBodies(bodies);
        Gdx.gl.glClearColor(BACKGROUND_RED, BACKGROUND_GREEN, BACKGROUND_BLUE, 1f);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        this.camera.update(delta);
        if (mapRenderer.isPresent()) {
            mapRenderer.get().setView((OrthographicCamera) this.camera);
            mapRenderer.get().render();
        }

        // Uncomment the following lines to see the debug collision shapes
        // if (world.isPresent()) {
        //    debugRenderer.render(this.world.get(), this.camera.getCombined());
        // }

        // Start to draw sprite

        if (player.isPresent() && !player.get().isQueuedForDestruction()) {
            spriteBatch.setProjectionMatrix(camera.getCombined());
            if (player.get().isJumping() && skinPlayer.isPresent() && skinPlayer.get().getState() != AnimationState.IS_JUMPING) {
                skinPlayer = Optional.ofNullable(new SkinFactoryEntityImpl<>()
                        .createSkinWithState(player.get(), AnimationState.IS_JUMPING));
            } else if (skinPlayer.isPresent() && skinPlayer.get().getState() != AnimationState.IS_IDLE && !player.get().isJumping()) {
                skinPlayer = Optional.ofNullable(new SkinFactoryEntityImpl<>()
                        .createSkinWithState(player.get(), AnimationState.IS_IDLE));
            }
            skinPlayer.get().swichFrameByTime();
            spriteBatch.begin();
            skinPlayer.get().drawOnSpriteBatch(spriteBatch, player.get().getPosition());
            spriteBatch.end();
        }

        if (enemies.isPresent()) {
            enemies.get().forEach((Robot robot) -> {
                if (!robot.isQueuedForDestruction()) {
                    skinEnemy.get().swichFrameByTime();
                    spriteBatch.begin();
                    skinEnemy.get().drawOnSpriteBatch(spriteBatch, robot.getPosition());
                    spriteBatch.end();
                }
            });
        }
        bodies.forEach((body) -> {
            final Object userData = body.getUserData();
            if (userData instanceof Bullet) {
                if (skinBullet.isPresent()) {
                    skinBullet.get().swichFrameByTime();
                    spriteBatch.begin();
                    skinBullet.get().drawOnSpriteBatch(spriteBatch, ((Bullet) userData).getPosition());
                    spriteBatch.end();

                } else {
                    this.setBulletSkin((Bullet) userData);
                }

            }
        });

        // End to draw sprite
    }

    @Override
    public final void setTiledMap(final TiledMap tiledMap) {
        mapRenderer = Optional.of(new OrthogonalTiledMapRenderer(tiledMap, UNITY_SCALE));

    }

    @Override
    public final void setWorld(final World world) {
        this.world = Optional.of(world);
    }

    @Override
    public final Camera getCamera() {
        return camera;
    }

    @Override
    public final void setPlayer(final Player player) {
        this.player = Optional.of(player);
        skinPlayer = Optional.ofNullable(new SkinFactoryEntityImpl<Player>().createSkin(player));

    }

    //polling problem
    @Override
    public final void setEnemySkin(final Wave wave) {
        this.enemies = Optional.of(wave.getRobots());
        if (this.enemies.isPresent()) {
            skinEnemy = Optional.ofNullable(new SkinFactoryEntityImpl<Robot>()
                    .createSkin(this.enemies.get().stream().findFirst().get()));
        }
    }

    @Override
    public final void setBulletSkin(final Bullet entity) {

            skinBullet = Optional.ofNullable(new SkinFactoryObjectImpl<Bullet>().createSkin(entity));

    }

    @Override
    public void create() {
    }

    /**
     *needed to draw.
     * @return place to draw
     */
    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }

}
