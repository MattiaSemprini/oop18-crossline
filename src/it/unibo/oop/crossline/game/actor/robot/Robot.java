package it.unibo.oop.crossline.game.actor.robot;

import java.util.Observer;
import it.unibo.oop.crossline.game.actor.Actor;
import it.unibo.oop.crossline.game.attributes.Armed;
import it.unibo.oop.crossline.game.attributes.Physical;

/**
 * This class represents a robot, the main enemy of the player.
 */
public interface Robot extends Actor, Armed {

    /**
     * Add an observer to the robot that gets notified on death.
     * @param observer the observer
     */
    void addObserver(Observer observer);

    /**
     * Get the current target of the robot.
     * @return the target
     */
    Physical getTarget();

}
