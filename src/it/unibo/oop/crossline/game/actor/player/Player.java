package it.unibo.oop.crossline.game.actor.player;

import com.badlogic.gdx.math.Vector2;

import it.unibo.oop.crossline.game.actor.Actor;
import it.unibo.oop.crossline.game.attributes.Armed;
import it.unibo.oop.crossline.game.weapon.Weapon;

/**
 * This class creates methods for move, jump and shoot actions.
 */
public interface Player extends Actor, Armed {

    /**
     * Apply velocity to player.
     * 
     * @param direction  the direction to be applied
     */
    void move(Vector2 direction);

    /**
     * Apply linear impulse in the x axis.
     */
    void jump();

    /**
     * Check if the player can shoot.
     */
    void shoot();

    /**
     *Set the weapon to the player.
     *
     * @param weapon  the player weapon
     */
    void setWeapon(Weapon weapon);

    /**
     * Check if the player is jumping.
     * 
     * @return the jumping state
     */
    boolean isJumping();

    /**
     * Set jumping state.
     * 
     * @param jumping  the jumping state
     */
    void setJumpState(boolean jumping);
}
