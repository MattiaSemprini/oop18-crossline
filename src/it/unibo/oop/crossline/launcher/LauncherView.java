package it.unibo.oop.crossline.launcher;

import java.awt.Dimension;
import java.awt.event.ActionListener;

/**
 * View of the launcher.
 */
public interface LauncherView {

    /**
     * Get the selected resolution.
     * @return the resolution
     */
    Dimension getResolution();

    /**
     * Set the resolution field.
     * @param resolution the resolution to apply
     */
    void setResolution(Dimension resolution);

    /**
     * Get the current fullscreen checkbox status.
     * @return the checkbox status
     */
    boolean isFullscreen();

    /**
     * Set the fullscreen checkbox.
     * @param fullscreen the fullscreen status
     */
    void setFullscreen(boolean fullscreen);

    /**
     * Set the the Play button listener.
     * @param listener the actions listener
     */
    void setPlayListener(ActionListener listener);

    /**
     * Set the launcher visibility.
     * @param visibility the visibility status
     */
    void setVisibility(boolean visibility);
}
